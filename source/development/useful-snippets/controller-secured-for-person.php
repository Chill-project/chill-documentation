<?php

namespace Chill\HealthBundle\Controller;

use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Security\Core\Role\Role;

class ConsultationController extends Controller
{
    /**
     * 
     * @param int $id personId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws type
     */
    public function listAction($id)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        /* @var $authorizationHelper \Chill\MainBundle\Security\Authorization\AuthorizationHelper */
        $authorizationHelper = $this->get('chill.main.security.'
            . 'authorization.helper');
        
        $circles = $authorizationHelper->getReachableCircles(
            $this->getUser(), 
            new Role(ConsultationVoter::SEE), 
            $person->getCenter()
            );
        
        // create a query which take circles into account        
        $consultations = $this->getDoctrine()->getManager()
            ->createQuery('SELECT c FROM ChillHealthBundle:Consultation c '
                . 'WHERE c.patient = :person AND c.circle IN(:circles) '
                . 'ORDER BY c.date DESC')
            ->setParameter('person', $person)
            ->setParameter('circles', $circles)
            ->getResult();
        
        return $this->render('ChillHealthBundle:Consultation:list.html.twig', array(
                'person' => $person,
                'consultations' => $consultations
            ));    
    }
}

