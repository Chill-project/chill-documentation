.. chill-doc documentation master file, created by
   sphinx-quickstart on Sun Sep 28 22:04:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Welcome to Chill documentation!
=====================================

Chill is a free software for social workers.

Chill rely on the php framework `Symfony <http://symfony.com>`_. 

Contents of this documentation:

.. toctree::
   :maxdepth: 2

   installation/index.rst
   development/index.rst
   Bundles <bundles/index.rst>

Let's talk together !
======================

Subscribe to the dev mailing-list to discuss your project and extend Chill with the feature you need! 

- `The dev mailing-list <https://lists.chill.social/listinfo/dev>`_
- `Read the archives of the mailing-list <https://lists.chill.social/pipermail/dev/>`_



Contribute
==========


* `Issue tracker <https://git.framasoft.org/groups/Chill-project/issues>`_ You may want to dispatch the issue in the multiple projects. If you do not know in which project is located your bug / feature request, use the project Chill-Main.
* `The dev mailing-list <https://lists.chill.social/listinfo/dev>`_

Source code is dispatched in multiple bundle, to improve re-usability. Each bundle has dependencies with other chill bundle, but the developer take care that bundles may not be installed if the user do not need it.

User manual
===========

An user manual exists in French and currently focuses on describing the main concept of the software. 

`Read (and contribute) to the manual <https://fr.wikibooks.org/wiki/Chill>`_

Available bundles
=================

* Chill-standard | https://git.framasoft.org/Chill-project/Chill-Standard This is the skeleton of the project. It does contains only few code, but information about configuration of your instance ;
* Chill-Main : https://git.framasoft.org/Chill-project/Chill-Main : the main, required bundle for all the subsequent chill bundles. It contains the framework to add features (like searching, timeline, ...). It also provides the user managements (authentification and authorization)  ;
* Chill-Person : https://git.framasoft.org/Chill-project/Chill-Person This is the bundle which provides the possibility to create and add a person.
* Chill-CustomFields : https://git.framasoft.org/Chill-project/Chill-CustomFields This bundle allows you to create custom fields on other bundles. It provides the framework for this features, and modify the schema according to this. It is required by Chill-Person and Chill-Report.
* Chill-Report: https://git.framasoft.org/Chill-project/Chill-Report This bundle allow to add report about People recorded in your database ;
* Chill-Activity : https://git.framasoft.org/Chill-project/Chill-Activity This bundle allow to add activities about People recorded in your database ;
* Chill-ICPC2 : https://git.framasoft.org/Chill-project/Chill-ICPC2 This bundle provides a custom fields for `ICPC code <https://en.wikipedia.org/wiki/International_Classification_of_Primary_Care>`_ (international classification for primary care)
* Chill-Group: https://git.framasoft.org/Chill-project/Chill-Group This bundle provides a way to create link between accompanyed people
* Chill-Event: https://git.framasoft.org/Chill-project/Chill-Event This bundle provides a way to create event and associate people to event through a "participation"
* Chill-Ldap: https://git.framasoft.org/Chill-project/Chill-Ldap Allow to synchronize the database with a ldap directory.
* Chill-ONEStat : https://framagit.org/Chill-project/Chill-ONEStat Provide statistics for the Belgian one "Centre de vacances" and "Ecoles de devoir".

You will also found the following projects :

* The present documentation : https://git.framasoft.org/Chill-project/chill-documentation
* The website https://chill.social : https://git.framasoft.org/Chill-project/chill.social

And various project to build docker containers with Chill.

TODO in documentation
=====================

.. todolist::

Licence
========

The project is available under the `GNU AFFERO GENERAL PUBLIC LICENSE v3`_.

This documentation is published under the `GNU Free Documentation License (FDL) v1.3`_


.. _GNU AFFERO GENERAL PUBLIC LICENSE v3: http://www.gnu.org/licenses/agpl-3.0.html
.. _GNU Free Documentation License (FDL) v1.3: http://www.gnu.org/licenses/fdl-1.3.html

