<?php

#Chill\MainBundle\DependencyInjection\ChillMainExtension.php

namespace Chill\MainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\MainBundle\DependencyInjection\Widget\Factory\WidgetFactoryInterface;
use Chill\MainBundle\DependencyInjection\Configuration;

/**
 * This class load config for chillMainExtension.
 */
class ChillMainExtension extends Extension implements Widget\HasWidgetFactoriesExtensionInterface
{
    /**
     * widget factory
     * 
     * @var WidgetFactoryInterface[]
     */
    protected $widgetFactories = array();
    
    public function addWidgetFactory(WidgetFactoryInterface $factory)
    {
        $this->widgetFactories[] = $factory;
    }
    
    /**
     * 
     * @return WidgetFactoryInterface[]
     */
    public function getWidgetFactories()
    {
        return $this->widgetFactories;
    }
    
    public function load(array $configs, ContainerBuilder $container)
    {
        // configuration for main bundle
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);
        
        // add the key 'widget' without the key 'enable'
        $container->setParameter('chill_main.widgets', 
            array('homepage' => $config['widgets']['homepage']));

        // ... 
    }
    
    public function getConfiguration(array $config, ContainerBuilder $container)
    {
        return new Configuration($this->widgetFactories, $container);
    }
    
}
