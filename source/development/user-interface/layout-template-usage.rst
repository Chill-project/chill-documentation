.. Copyright (C)  2015 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".


Layout / Template usage
#######################

We recommand the use of the existing layouts to ensure the consistency of the design. This section explains the different templates and how to use it.

The layouts are twig templates.

Twig templating helper
======================

`chill_print_or_message`
------------------------

Print a value or use a default template if the value is empty.

The template can be customized. 

Two default templates are registered: 

- :code:`default`, do not decorate the value ;
- :code:`blockquote`: wrap the value into a blockquote if exists

.. code-block:: html+twig

  {{ "This is a message"|chill_print_or_message("No message") }} 
  <!-- will print "This is a message" -->

  {{ ""|chill_print_or_message("No message")}} 
  <!-- will print <span class="chill-no-data-statement">No message</span> -->

  {{ "This is a comment\n with multiples lines"|chill_print_or_message("No comment", 'blockquote') }} 
  <!-- will print <blockquote class="chill-user-quote">This is a comment<br/>with multiples lines</blockquote> -->

When customizing the template, two arguments are passed to the template: 

- :code:`value`: the actual value ;
- :code:`message`: the message, given as argument

Routing with return path
------------------------

Three twig function are available for creating routes and handling "return path".

**Rationale**: When building a "CRUD" (CReate, Update, Delete of an entity), you would like to allow people to go back on some custom cancel page when coming for another place of an application. For instance, if you are in the timeline and show an activity, you would like that the user go back to the timeline when pressing the "return" button at the bottom of the page, instead of going back to the "activity list" page.

Using those function, a :code:`returnPath` parameter will be added in the path generated. It will be used instead of the default one in the subsequente pages. 

- :code:`chill_path_add_return_path(name, parameters = [], relative = false)`: will create a path with current page as return path (users will go back to the current page on the next page) ;
- :code:`chill_return_path_or(name, parameters = [], relative = false)`: will create a path to the return path if present, or build the path according to the given parameters ;
- :code:`chill_path_forward_return_path(name, parameters = [], relative = false)`: will create a path and adding the return path that is present for the current page, *forwarding* the return path to the next page. This is useful if you are on a "show" page with a return path (by instance, the "timeline" page), you place a link to the *edit* page and want users to go back to the "timeline" page on the edit page. 


- :code:`chill_return_path_or`: 


Organisation of the layouts
===========================

ChillMainBundle::layout.html.twig
---------------------------------

This is the base layout. It includes the most import css / js files. It display a page with

* a horizontal navigation menu
* a place for content
* a footer


The layout containts blocks, that are :

* title

  * to display title

* css

  * where to add some custom css

* navigation_section_menu

  * place where to insert the section menu in the navigation menu (by default the navigation menu is inserted)

* navigation_search_bar

  * place where to insert a search bar in the navigation menu (by default the search bar is inserted)

* top_banner

  * place where to display a banner below the navigation menu (this place is use to display the details of the person)

* sublayout_containter

  * place between the header and the footer that can be used to create a new layout (with vertical menu for example)

* content

  * place where to display the content (flash message are included outside of this block)

* js

  * where to add some custom javascript


ChillMainBundle::layoutWithVerticalMenu.html.twig
-------------------------------------------------

This layout extends `ChillMainBundle::layout.html.twig`. It replaces the block `layout_content` and divides this block for displaying a vertical menu and some content.

It proposes 2 new blocks :

* layout_wvm_content

  * where to display the page content

* vertical_menu_content

  * where to place the vertical menu


ChillMainBundle::Admin/layout.html.twig
---------------------------------------

This layout extends `ChillMainBundle::layout.html.twig`. It hides the search bar, remplaces the `section menu` with the `admin section menu`.

It proposes a new block :

* admin_content

  * where to display the admin content


ChillMainBundle::Admin/layoutWithVerticalMenu.html.twig
-------------------------------------------------------

This layout extends `ChillMainBundle::layoutWithVerticalMenu.html.twig`. It do the same changes than `ChillMainBundle::Admin/layout.html.twig` : hiding the search bar, remplacing the `section menu` with the `admin section menu`.

It proposes a new block :

* admin_content

  * where to display the admin content

ChillPersonBundle::layout.html.twig
-----------------------------------

This layout extend `ChillMainBundle::layoutWithVerticalMenu.html.twig` add the person details in the block `top_banner`, set the menu `person` as the vertical menu.

It proposes 1 new block :

* personcontent

  * where to display the information of the person


ChillMainBundle::Export/layout.html.twig
----------------------------------------

This layout extends `ChillMainBundle::layoutWithVerticalMenu.html.twig` and set the menu `export` as the vertical menu.

It proposes 1 new block :

* export_content

  * where to display the content of the export

Useful template and helpers
===========================

Macros
------

Every bundle may bring their own macro to print resources with uniformized styles. 

See : 

- :ref:`Macros in person bundle <person-bundle-macros>` ;
- :ref:`Macros in activity bundle <activity-bundle-macros>` ;
- :ref:`Macros in group bundle <group-bundle-macros>` ;
- :ref:`Macros in main bundle <main-bundle-macros>` ;

Templates
---------

ChillMainBundle::Util:confirmation_template.html.twig
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This template show a confirmation template before making dangerous things. You can add your own message and title, or define those message by yourself in another template.

The accepted parameters are : 

- `title` (string) a title for the page. Not mandatory (it won't be rendered if not defined)
- `confirm_question` (string) a confirmation question. This question will not be translated into the template, and may be printed as raw. Not mandatory (it won't be rendered if not defined)
- `form` : (:class:`Symfony\Component\Form\FormView`) a form wich **must** contains an input named `submit`, which must be a :class:`Symfony\Component\Form\Extension\Core\Type\SubmitType`. Mandatory
- `cancel_route` : (string) the name of a route if the user want to cancel the action
- `cancel_parameters` (array) the parameters for the route defined in `cancel_route`


Usage : 


.. code-block:: html+twig

       {{ include('ChillMainBundle:Util:confirmation_template.html.twig',
           {
               # a title, not mandatory
               'title'             : 'Remove membership'|trans,
               # a confirmation question, not mandatory
               'confirm_question'  : 'Are you sure you want to remove membership ?'|trans
               # a route for "cancel" button (mandatory)
               'cancel_route'      : 'chill_group_membership_by_person',
               # the parameters for 'cancel' route (default to {} )
               'cancel_parameters' : { 'person_id' : membership.person.id },
               # the form which will send the deletion. This form
               # **must** contains a SubmitType
               'form'              : form
           } ) }}
