<?php

# Chill/PersonBundle/DependencyInjection/ChillPersonExtension.php

namespace Chill\PersonBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\MainBundle\DependencyInjection\MissingBundleException;
use Chill\PersonBundle\Security\Authorization\PersonVoter;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillPersonExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
      // ...
    }
    
    
    /**
     * 
     * Add a widget "add a person" on the homepage, automatically
     * 
     * @param \Chill\PersonBundle\DependencyInjection\containerBuilder $container
     */
    public function prepend(ContainerBuilder $container) 
    {
        $container->prependExtensionConfig('chill_main', array(
            'widgets' => array(
                'homepage' => array(
                    array(
                        'widget_alias' => 'add_person',
                        'order' => 2
                    )
                )
            )
        ));
    }
}

