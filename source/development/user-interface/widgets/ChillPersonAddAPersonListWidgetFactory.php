<?php

# Chill/PersonBundle/Widget/PersonListWidgetFactory

namespace Chill\PersonBundle\Widget;

use Chill\MainBundle\DependencyInjection\Widget\Factory\AbstractWidgetFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;

/**
 * add configuration for the person_list widget.
 */
class PersonListWidgetFactory extends AbstractWidgetFactory
{
   /*
    * append the option to the configuration
    * see http://symfony.com/doc/current/components/config/definition.html
    *
    */
    public function configureOptions($place, NodeBuilder $node)
    {
        $node->booleanNode('only_active')
                ->defaultTrue()
                ->end();
        $node->integerNode('number_of_items')
            ->defaultValue(50)
            ->end();
        $node->scalarNode('filtering_class')
                ->defaultNull()
                ->end();
            
    }
     
   /*
    * return an array with the allowed places where the widget can be rendered
    *
    * @return string[]
    */   
    public function getAllowedPlaces()
    {
        return array('homepage');
    }
    
   /* 
    * return the widget alias
    *
    * @return string
    */
    public function getWidgetAlias()
    {
        return 'person_list';
    }
    
    /*
     * return the service id for the service which will render the widget.
     *
     * this service must implements `Chill\MainBundle\Templating\Widget\WidgetInterface`
     * 
     * the service must exists in the container, and it is not required that the service 
     * has the `chill_main` tag.
     */
    public function getServiceId(ContainerBuilder $containerBuilder, $place, $order, array $config)
    {
        return 'chill_person.widget.person_list';
    }
    
}

