.. chill-doc documentation master file, created by
   sphinx-quickstart on Sun Sep 28 22:04:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Copyright (C)  2014-2019 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

Installation & Usage
####################


Requirements
************

- This project use `docker  <https://docker.com>`_ to be run. As a developer, use `docker-compose <https://docs.docker.com/compose/overview/>`_ to bootstrap a dev environment in a glance. You do not need any other dependencies ;
- Make is used to automate scripts.

Installation in development mode
********************************

1. Get the code
===============

Clone or download the chill-standard project and `cd` into the main directory.


.. code-block:: bash

   git clone https://framagit.org/Chill-project/Chill-Standard.git
   cd Chill-Standard


As a developer, the code will stay on your computer and will be executed in docker container. To avoid permission problem, the code should be run with the same uid/gid from your current user. This is why we get your current user id with the command ``id -u`` in each following scripts.

2. Prepare your variables
=========================

Have a look at the variable in ``env.dist`` and in ``app/config/parameters.yml.dist`` and check if you need to adapt them. If they do not adapt with your need, or if some are missing: 

1. copy the file as ``.env``: ``cp env.dist .env``
2. replace the variable inside ``.env``
3. copy the file ``parameters.yml.dist``: ``cp app/config/parameters.yml.dist app/config/parameters``
4. replace eventually the variable inside this file

**Note**: If you intend to use the bundle ``Chill-Doc-Store``, you will need to configure and install an openstack object storage container with temporary url middleware. You will have to configure `secret keys <https://docs.openstack.org/swift/latest/api/temporary_url_middleware.html#secret-keys>`_.

3. Run the bootstrap script
===========================

This script can be run using `make`

.. code-block:: bash

   make init

This script will :

1. force docker-compose to, eventually, pull the base images and build the image used by this project ;
2. run an install script to download `composer <https://getcomposer.org>`_ ; 
3. install the php dependencies
4. build assets


4. Start the project
====================

.. code-block:: bash

   docker-compose up

**On the first run** (and after each upgrade), you must execute *post update commands* and run database migrations. With a container up and running, execute the following commands:

.. code-block:: bash

   make migrate

Chill will be available at ``http://localhost:8001.`` Currently, there isn't any user or data. To add fixtures, run

.. code-block:: bash

   docker-compose exec --user $(id -u) php app/console doctrine:fixtures:load

There are several users available:

- ``center a_social``
- ``center b_social``

The password is always ``password``.

Now, read `Operations` below.

Operations
**********

Build assets
============

run those commands: 

.. code-block:: bash

   make build-assets

How to execute the console ?
============================

.. code-block:: bash

   # if a container is running
   docker-compose exec --user $(id -u) php app/console
   # if not 
   docker-compose run --user $(id -u) php app/console

How to create the database schema (= run migrations) ?
======================================================

.. code-block:: bash

   # if a container is running
   docker-compose exec --user $(id -u) php app/console doctrine:migrations:migrate
   # if not 
   docker-compose run --user $(id -u) php app/console doctrine:migrations:migrate


How to read the email sent by the program ?
===========================================

Go at ``http://localhost:8005`` and you should have access to mailcatcher.

In case of you should click on a link in the email, be aware that you should remove the "s" from https.

How to load fixtures ? (development mode only)
==============================================

.. code-block:: bash

   # if a container is running
   docker-compose exec --user $(id -u) php app/console doctrine:fixtures:load
   # if not 
   docker-compose run --user $(id -u) php app/console doctrine:fixtures:load

How to open a terminal in the project
=====================================

.. code-block:: bash

   # if a container is running
   docker-compose exec --user $(id -u) php /bin/bash
   # if not 
   docker-compose run --user $(id -u) php /bin/bash

How to run composer ?
=====================

.. code-block:: bash

   # if a container is running
   docker-compose exec --user $(id -u) php ./composer.phar
   # if not 
   docker-compose run --user $(id -u) php ./composer.phar

How to access to PGADMIN ?
==========================

Pgadmin is installed with docker-compose.

You can access it at ``http://localhost:8002``.

Credentials:

- login: admin@chill.social
- password: password

How to run tests ?
==================

Tests reside inside the installed bundles. You must `cd` into that directory, download the required packages, and execute them from this place.

**Note**: some bundle require the fixture to be executed. See the dedicated _how-tos_.

Exemple, for running test inside `main` bundle: 

.. code-block:: bash

   # mount into the php image
   docker-compose run --user $(id -u) php /bin/bash
   # cd into main directory
   cd vendor/chill-project/main
   # download deps
   php ../../../composer.phar install
   # run tests
   /vendor/bin/phpunit

How to run webpack interactively
================================

Executing :code:`bash docker-node.sh` will open a terminal in a node container, with volumes mounted.

Build the documentation API
===========================

A basic configuration of `sami <https://github.com/FriendsOfPhp/Sami>`_ is embedded within the project. 

A configuration file for `phpDocumentor <https://www.phpdoc.org>`_ is present.

Error `An exception has been thrown during the rendering of a template ("Asset manifest file "/var/www/app/web/build/manifest.json" does not exist.").` on first run
====================================================================================================================================================================

Run :code:`make build-assets`

Running in production
*********************

Currently, to run this software in production, the *state of the art* is the following :

1. Run the software locally and tweak the configuration to your needs ;
2. Build the image and store them into a private container registry. This can be done using :code:`make build-and-push-image`.
   
   To be sure to target the correct container registry, you have to adapt the values ``IMAGE_NGINX`` and ``IMAGE_PHP`` date in the ``.env`` file.
3. Run the image on your production server, using docker-compose or eventually docker stack. You have to customize the variable set in docker-compose.

   See also the :ref:`running-production-tips-and-tricks` below.

.. warning::

   In production, you **must** set those variables:

   * ``APP_ENV`` to ``prod``
   * ``APP_DEBUG`` to ``false``

   There are security issues if you keep the same variable than for production.


.. _running-production-tips-and-tricks:

Tips and tricks
===============

Operation on database (backups, running custom sql, replication) are easier to set when run outside of a container. If you run into a container, take care of the volume where data are stored.

The PHP sessions are stored inside redis. This is useful if you distribute the traffic amongst different php server: they will share same sessions if a request goes into a different instance of the container.

When the PHP servers are shared across multiple instances, take care that some data is stored into redis: the same redis server should be reachable by all instances.

It is worth having an eye on the configuration of logstash container.


Design principles
*****************

Why the DB URL is set in environment, and not in parameters.yml ?
=================================================================

Because, at startup, a script does check the db is up and, if not, wait for a couple of seconds before running ``entrypoint.sh``. For avoiding double configuration, the configuration of the PHP app takes his configuration from environment also (and it will be standard in future releases, with symfony 4.0).

