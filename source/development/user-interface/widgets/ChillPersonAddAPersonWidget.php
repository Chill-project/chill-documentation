<?php
# Chill/PersonBundle/Widget/PersonListWidget.php

namespace Chill\PersonBundle\Widget;

use Chill\MainBundle\Templating\Widget\WidgetInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\DBAL\Types\Type;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Security\Core\Role\Role;
use Doctrine\ORM\EntityManager;

/**
 * add a widget with person list. 
 * 
 * The configuration is defined by `PersonListWidgetFactory`
 */
class PersonListWidget implements WidgetInterface
{
    
    /**
     * Repository for persons
     * 
     * @var EntityRepository
     */
    protected $personRepository;
    
    /**
     * The entity manager
     *
     * @var EntityManager
     */
    protected $entityManager;
    
    /**
     * the authorization helper
     * 
     * @var AuthorizationHelper;
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var TokenStorage
     */
    protected $tokenStorage;
    
    /**
     *
     * @var UserInterface
     */
    protected $user;
    
    public function __construct(
            EntityRepository $personRepostory,
            EntityManager $em,
            AuthorizationHelper $authorizationHelper,
            TokenStorage $tokenStorage
            ) {
        $this->personRepository = $personRepostory;
        $this->authorizationHelper = $authorizationHelper;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $em;
    }

    /**
     * 
     * @param type $place
     * @param array $context
     * @param array $config
     * @return string
     */
    public function render(\Twig_Environment $env, $place, array $context, array $config)
    { 
        $qb = $this->personRepository
                ->createQueryBuilder('person');
        
        // show only the person from the authorized centers
        $and = $qb->expr()->andX();
        $centers = $this->authorizationHelper
                ->getReachableCenters($this->getUser(), new Role(PersonVoter::SEE));
        $and->add($qb->expr()->in('person.center', ':centers'));
        $qb->setParameter('centers', $centers);
        

        // add the "only active" where clause
        if ($config['only_active'] === true) {
            $qb->join('person.accompanyingPeriods', 'ap');
            $or = new Expr\Orx();
            // add the case where closingDate IS NULL
            $andWhenClosingDateIsNull = new Expr\Andx();
            $andWhenClosingDateIsNull->add((new Expr())->isNull('ap.closingDate'));
            $andWhenClosingDateIsNull->add((new Expr())->gte(':now', 'ap.openingDate'));
            $or->add($andWhenClosingDateIsNull);
            // add the case when now is between opening date and closing date
            $or->add(
                    (new Expr())->between(':now', 'ap.openingDate', 'ap.closingDate')
                );
            $and->add($or);
            $qb->setParameter('now', new \DateTime(), Type::DATE);
        }
        
        // adding the where clause to the query
        $qb->where($and);
        
        $qb->setFirstResult(0)->setMaxResults($config['number_of_items']);
        
        $persons = $qb->getQuery()->getResult();
        
        return $env->render(
            'ChillPersonBundle:Widget:homepage_person_list.html.twig',
            array('persons' => $persons)
            );
    }
    
    /**
     * 
     * @return UserInterface
     */
    private function getUser()
    {
        // return a user
    }

}
