<?php

namespace Chill\MyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ItemController extends Controller {

    public function yourAction() 
    {
        $em = $this->getDoctrine()->getManager();
        // first, get the number of total item are available
        $total = $em
             ->createQuery("SELECT COUNT (item.id) FROM ChillMyBundle:Item item")
             ->getSingleScalarResult();
       
        // get the PaginatorFactory
        $paginatorFactory = $this->get('chill_main.paginator_factory');
        
        // create a pagination instance. This instance is only valid for 
        // the current route and parameters
        $paginator = $paginatorFactory->create($total);
        
        // launch your query on item. Limit the query to the results
        // for the current page using the paginator
        $items = $em->createQuery("SELECT item FROM ChillMyBundle:Item item WHERE <your clause>")
            // use the paginator to get the first item number
            ->setFirstResult($paginator->getCurrentPage()->getFirstItemNumber())
            // use the paginator to get the number of items to display
            ->setMaxResults($paginator->getItemsPerPage());
            
        return $this->render('ChillMyBundle:Item:list.html.twig', array(
                'items' => $items,
                'paginator' => $paginator
                );
            
    }

}

