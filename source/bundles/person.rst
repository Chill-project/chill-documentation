.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _person-bundle:

Person bundle
#############

This bundle provides the ability to record people in the software. This bundle is required by other bundle.

.. contents:: Table of content
   :local:

Entities provided
*****************

.. todo::

   describe entities provided by person bundle
   
   
Search terms
************

The class `Chill\PersonBundle\Search\PersonSearch` provide the search module.

Domain
======

The search upon "person" is provided by default. The `@person` domain search may be omitted.

* `@person` is the domain search for people.


Arguments
=========

* `firstname` : provide the search on firstname. Example : `firstname:Depardieu`. May match part of the firstname (`firsname:dep` will match Depardieu)
* `lastname` : provide the search on lastname. May match part of the lastname.
* `birthdate` : provide the search on the birthdate. Example : `birthdate:1996-01-19`
* `gender`: performs search on man/woman. The accepted values are `man` or `woman`.
* `nationality` : performs search on nationality. Value must be a country code `as described in ISO 3166 <http://www.iso.org/iso/fr/home/standards/country_codes.htm>`_. Example : `nationality:FR`.

Default
=======

The default search is performed on firstname and/or lastname. Both are concatened before search. If values are separated by spaces, the clause `AND` is used : the search `dep ge` will match 'Gérard Depardieu` or 'Jean Depagelles', but not 'Charline Depardieu' (missing 'Ge' in word).

Configuration options
*********************

Those options are available under `chill_person` key.

Example of configuration:

.. code-block:: yaml

   chill_person:
      validation:
         birthdate_not_after: P15Y
      person_fields:
         # note: visible is the default config. This key may be omitted if visible is chosen.
         nationality: hidden
         email: hidden
         place_of_birth: visible
         phonenumber: hidden
         country_of_birth: hidden
         marital_status: visible
         spoken_languages: hidden
         address: visible

birthdate_not_after *string*
   The period duration before today during which encoding birthdate is not possible. The period is a string matching the format of `ISO_8601`, which is also use to build `DateInterval classes <http://php.net/manual/en/dateinterval.construct.php>`_.

   Example: `P1D`, `P18Y`

   Default value: `P1D` which means that birthdate before the current day (= yesterday) are allowed.

person_fields *array*
   This define the visibility of some fields. By default, all fields are visible, but you can choose to hide some of them. Available keys are : 
   
   * `nationality`
   * `country_of_birth`
   * `place_of_birth`
   * `phonenumber`
   * `email`
   * `marital_status`
   * `spoken_languages`
   * `address`

   Possibles values: `hidden` or `visible` (all other value will raise an Exception).

   Default value : `visible`, which means that all fields are visible.

   Example: 

   .. code-block:: yaml

      chill_person:
         person_fields:
            nationality: hidden
            email: hidden
            phonenumber: hidden

.. note:: 
   If all the field of a "box" are hidden, the whole box does not appears. Example: if the fields `phonenumber` and `email` are hidden, the title `Contact information` will be hidden in the UI.

.. note:: 
   If you hide multiple fields, for a better integration you may want to override the template, for a better appeareance. See `the symfony documentation <http://symfony.com/doc/current/book/templating.html#overriding-bundle-templates>`_ about this feature.

.. _person-bundle-macros:

Macros
******

Sticker for a person
=====================

Macro file
   `ChillPersonBundle:Person:macro.html.twig`
Macro envelope
   :code:`render(p, withLink=false)`

   :code:`p` is an instance of :class:`Chill\PersonBundle\Entity\Person`

   :code:`withLink` :class:`boolean`
When to use this macro ?
   When you want to represent a person.
Example usage :
   .. code-block:: html+jinja

      {% import "ChillPersonBundle:Person:macro.html.twig" as person_ %}

      {{ person_.render(person, true) }}

Layout events and delegated blocks
***********************************

:code:`chill_block.person_post_vertical_menu` event
====================================================

This event is available to add content below of the vertical menu (on the right).

The context is : 

- :code:`person` : the current person which is rendered. Instance of :class:`Chill\PersonBundle\Entity\Person`

Widgets
*******

Add a list of person on homepage
================================

The bundle provide a way to add a list of accompanyied person on the homepage:

.. code-block:: yaml

   chill_main:
       widgets:
           homepage: 
               - 
                   order: 10
                   widget_alias: person_list
                   person_list:
                       # customize the number of items
                       number_of_items: 20

                       # only active
                       only_active: true

                       # you can add some filtering class, which will implements
                       # Chill\PersonBundle\PersonListWidget\PersonFilteringInterface
                       filtering_class: "\Hepc\HomepagePersonFiltering"

                       # when the view is overriden, you can add some custom fields
                       # to the view
                       custom_fields: [school-2fb5440e-192c-11e6-b2fd-74d02b0c9b55]
  
Commands
********


:code:`chill:person:move`
=========================

.. code-block:: txt

	Usage:
		chill:person:move [options]

	Options:
		-f, --from=FROM       The person id to delete, all associated data will be moved before
		-t, --to=TO           The person id which will received data
				--dump-sql        dump sql to stdout
				--force           execute sql instead of dumping it
		-h, --help            Display this help message
		-q, --quiet           Do not output any message
		-V, --version         Display this application version
				--ansi            Force ANSI output
				--no-ansi         Disable ANSI output
		-n, --no-interaction  Do not ask any interactive question
		-e, --env=ENV         The Environment name. [default: "dev"]
				--no-debug        Switches off debug mode.
		-v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

	Help:
		Move all the associated entities on a "from" person to a "to" person and remove the old person

Move all the entities associated to a person onto another one, and remove the old person.

.. warning:: 

  Some entities are ignored and will be deleted: 

  - the accompanying periods ;
  - the data attached to a person entity: name, address, date of birth, etc. Thos should be merge before the move.

It is advised to run first the command with the :code:`dump-sql` option and, then, use the :code:`force` option.

The moving and suppression is executed inside a transaction, ensuring no data loss if the migration fails.

.. note::

  Using bash and awk, it is easy to use a TSV file (values separated by a tab, not a comma) to create move commands. Assuming our file is named :code:`twins.tsv` and contains two columns: the first one with :code:`from` ids, and the second one with :code:`to` ids: 

  .. code-block:: bash

    awk '{ print "php app/console chill:person:move --dump-sql --from " $1 " --to " $2;}' twins.tsv 
