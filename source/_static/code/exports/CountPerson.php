<?php


namespace Chill\PersonBundle\Export\Export;

use Chill\MainBundle\Export\ExportInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\Query;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Security\Core\Role\Role;
use Chill\PersonBundle\Export\Declarations;
use Chill\MainBundle\Export\FormatterInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CountPerson implements ExportInterface
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    public function __construct(
            EntityManagerInterface $em
            )
    {
        $this->entityManager = $em;
    }
    
    public function getType()
    {
        return Declarations::PERSON_TYPE;
    }
    
    public function getDescription()
    {
        return "Count peoples by various parameters.";
    }
    
    public function getTitle()
    {
        return "Count peoples";
    }
    
    public function requiredRole()
    {
        return new Role(PersonVoter::STATS);
    }
    
    public function initiateQuery(array $requiredModifiers,  array $acl, array $data = array())
    {
        // we gather all center the user choose.
        $centers = array_map(function($el) { return $el['center']; }, $acl);
        
        $qb = $this->entityManager->createQueryBuilder();
        
        $qb->select('COUNT(person.id) AS export_result')
                ->from('ChillPersonBundle:Person', 'person')
                ->join('person.center', 'center')
                ->andWhere('center IN (:authorized_centers)')
                ->setParameter('authorized_centers', $centers);
                ;
        
        
        return $qb;
    }
    
    public function getResult($qb, $data)
    {
        return $qb->getQuery()->getResult(Query::HYDRATE_SCALAR);
    }
    
    public function getQueryKeys($data)
    {
        // this array match the result keys in the query. We have only
        // one column.
        return array('export_result');
    }
    
    public function getLabels($key, array $values, $data)
    {
        
        // the Closure which will be executed by the formatter. 
        return function($value) {
            switch($value) {
                case '_header':
                  // we have to process specifically the '_header' string, 
                  // which will be used by the formatter to show a column title
                  return $this->getTitle();
                default:
                  // for all value, we do not process them and return them
                  // immediatly
                  return $value;
        };
    }
    
    public function getAllowedFormattersTypes()
    {
        return array(FormatterInterface::TYPE_TABULAR);
    }
    
    public function buildForm(FormBuilderInterface $builder) {
        // this export does not add any form
    }
    
    public function supportsModifiers()
    {
        // explain the export manager which formatters and filters are allowed
        return array(Declarations::PERSON_TYPE, Declarations::PERSON_IMPLIED_IN);
    }
    
}
