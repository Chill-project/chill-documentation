<?php
   # Chill\MainBundle\DependencyInjection\Configuration.php
  
   namespace Chill\MainBundle\DependencyInjection;

   use Symfony\Component\Config\Definition\Builder\TreeBuilder;
   use Symfony\Component\Config\Definition\ConfigurationInterface;
   use Chill\MainBundle\DependencyInjection\Widget\AddWidgetConfigurationTrait;
   use Symfony\Component\DependencyInjection\ContainerBuilder;

   /**
    * Configure the main bundle
    */
   class Configuration implements ConfigurationInterface
   {
       
       use AddWidgetConfigurationTrait;
       
       /**
	*
	* @var ContainerBuilder
	*/
       private $containerBuilder;

       
       public function __construct(array $widgetFactories = array(), 
	       ContainerBuilder $containerBuilder)
       {
           // we register here widget factories (see below)
	   $this->setWidgetFactories($widgetFactories);
           // we will need the container builder later...
	   $this->containerBuilder = $containerBuilder;
       }
       
       /**
	* {@inheritDoc}
	*/
       public function getConfigTreeBuilder()
       {
	   $treeBuilder = new TreeBuilder();
	   $rootNode = $treeBuilder->root('chill_main');

	   $rootNode
	       ->children()

                   // ...

		   ->arrayNode('widgets')
		       ->canBeDisabled()
		       ->children()
                            // we declare here all configuration for homepage place
			    ->append($this->addWidgetsConfiguration('homepage', $this->containerBuilder))
		       ->end() // end of widgets/children
		   ->end() // end of widgets
		  ->end() // end of root/children
	       ->end() // end of root
	   ;

	   
	   return $treeBuilder;
       }
   }


