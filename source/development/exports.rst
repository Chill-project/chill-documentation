.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".


Exports
*******

Export is an important issue for the Chill software : users should be able to :

- compute statistics about their activity ;
- list "things" which make part of their activities.

The `main bundle`_ provides a powerful framework to build custom queries with re-usable parts across differents bundles.

.. contents:: Table of content
   :local:

.. seealso:: 

   `The issue where this framework was discussed <https://git.framasoft.org/Chill-project/Chill-Main/issues/9>`_
      Provides some information about the pursued features and architecture.

Concepts
========


Some vocabulary: 3 "Export elements"
------------------------------------

Four terms are used for this framework : 

exports
   provides some basic operation on the date. Two kind of exports are available : 

   - computed data : it may be "the number of people", "the number of activities", "the duration of activities", ...
   - list data : it may be "the list of people", "the list of activity", ...

filters
   The filters make a filter on the date: it removes some information the user doesn't want to introduce in the computation done by export. In other word, filters make a filter...

   Example of filter: "people under 18 years olds", "activities between the 1st of June and the 31st December", ...

aggregators
   The aggregator aggregates the data into some group (some software use the term 'bucket'). 

   Example of aggregator : "group people by gender", "group people by nationality", "group activity by type", ...

formatters
   The formatters format the data into a :class:`Symfony\Component\HttpFoundation\Response`, which  will be returned "as is" by the controller to the web client. 

   Example of formatter: "format data as CSV", "format data as ods spreadsheet", ...

Anatomy of an export
---------------------

An export may be explained as a sentence, where each part of this sentence refers to one or multiple exports element. Examples : 

**Example 1**: Count the number of people having at least one activity in the last 12 month, and group them by nationality and gender, and format them in a CSV spreadsheet.

Here : 

- *count the number of people* is the export part
- *having at least one activity* is the filter part
- *group them by nationality* is the aggregator part
- *group them by gender* is a second aggregator part
- *format the date in a CSV spreadsheet* is the formatter part

Note that :

- aggregators, filters, exports and aggregators are cross-bundle. Here the bundle *activity* provides a filter which apply on an export provided by the person bundle ;
- there may exists multiple aggregator or filter for one export. Currently, only one export is allowed.

The result might be : 

+-----------------------+----------------+---------------------------+
| Nationality           | Gender         | Number of people          |
+=======================+================+===========================+
| Russian               | Male           | 12                        |
+-----------------------+----------------+---------------------------+
| Russian               | Female         | 24                        |
+-----------------------+----------------+---------------------------+
| France                | Male           | 110                       |
+-----------------------+----------------+---------------------------+
| France                | Female         | 150                       |
+-----------------------+----------------+---------------------------+

**Example 2**: Count the average duration of an activity with type "meeting", which occurs between the 1st of June and the 31st of December, group them by week, and format the data in a OpenDocument spreadsheet.

Here : 

- *count the average duration of an activity* is the export part
- *activity with type meeting* is a filter part
- *activity which occurs between the 1st of June and the 31st of December* is a filter
- *group them by week* is the aggregator part
- *format the date in an OpenDocument spreadsheet* is the formatter part

The result might be :

+-----------------------+----------------------+
| Week                  | Number of activities | 
+=======================+======================+
| 2015-10               | 10                   |
+-----------------------+----------------------+
| 2015-11               | 12                   |
+-----------------------+----------------------+
| 2015-12               | 10                   |
+-----------------------+----------------------+
| 2015-13               | 9                    |
+-----------------------+----------------------+

Authorization and exports
-------------------------

Exports, filters and aggregators should not make see data the user is not allowed to see.

In other words, developers are required to take care of user authorization for each export.

It should exists a special role that should be granted to users which are allowed to build exports. For more simplicity, this role should apply on center, and should not requires special circles.

How does the magic works ?
===========================

To build an export, we rely on the capacity of the database to execute queries with aggregate (i.e. GROUP BY) and filter (i.e. WHERE) instructions.

An export is an SQL query which is initiated by an export, and modified by aggregators and filters.

.. note:: 

   **Example**: Count the number of people having at least one activity in the last 12 month, and group them by nationality and gender

   1. The report initiate the query 

   .. code-block:: SQL

      SELECT count(people.*) FROM people

   2. The filter add a where and join clause : 

   .. code-block:: SQL

      SELECT count(people.*) FROM people 
         RIGHT JOIN activity 
         WHERE activity.date IS BETWEEN now AND 6 month ago

   3. The aggregator "nationality" add a GROUP BY clause and a column in the SELECT statement: 

   .. code-block:: sql

      SELECT people.nationality, count(people.*) FROM people 
         RIGHT JOIN activity 
         WHERE activity.date IS BETWEEN now AND 6 month ago 
         GROUP BY nationality

   4. The aggregator "gender" do the same job as the nationality aggregator : it adds a GROUP BY clause and a column in the SELECT statement : 

   .. code-block:: sql

      SELECT people.nationality, people.gender, count(people.*) 
         FROM people RIGHT JOIN activity 
         WHERE activity.date IS BETWEEN now AND 6 month ago 
         GROUP BY nationality, gender

Each filter, aggregator and filter may collect parameters from the user by providing a form. This form is appended to the export form. Here is an example.

.. figure:: /_static/screenshots/development/export_form-fullpage.png

   The screenshot show the export form for ``CountPeople`` (Nombre de personnes). The filter by date of birth is checked (*Filtrer par date de naissance de la personne*), which allow to show a subform, which is provided by the :class:`Chill\PersonBundle\Export\Filter\BirthdateFilter`. The other filter, which are unchecked, does not show the subform.

   Two aggregators are also checked : by Country of birth (*Aggréger les personnes par pays de naissance*, corresponding class is :class:`Chill\PersonBundle\Export\Aggregator\CountryOfBirthAggregator`, which also open a subform. The aggregator by gender (*Aggréger les personnes par genre*) is also checked, but there is no corresponding subform.

The Export Manager
------------------

The Export manager (:class:`Chill\MainBundle\Export\ExportManager` is the central class which register all exports, aggregators, filters and formatters.

The export manager is also responsible for orchestrating the whole export process, producing a :class:`Symfony\FrameworkBundle\HttpFoundation\Request` to each export request.


The export form step
--------------------

The form step allow to build a form, aggregating different parts of the module.

The building of forms is separated between different subform, which are responsible for rendering their part of the form (aggregators, filters, and export). 

.. figure:: /_static/puml/exports/form_steps.png
   :scale: 40%

The formatter form step
-----------------------

The formatter form is processed *after* the user filled the export form. It is built the same way, but receive in parameters the data entered by the user on the previous step (i.e. export form). It may then adapt it accordingly (example: show a list of columns selected in aggregators).

Processing the export
---------------------

The export process may be explained by this schema :

.. figure:: /_static/puml/exports/processing_export.png
   :scale: 40%

   (Click to enlarge)


Export, formatters and filters explained
========================================

Exports
-------

This is an example of the ``CountPerson`` export :

.. literalinclude:: /_static/code/exports/CountPerson.php
   :language: php
   :linenos:

* **Line 36**: the ``getType`` function return a string. This string will be used to find the aggregtors and filters which will apply to this export.
* **Line 41**: a simple description to help user to understand what your export does.
* **Line 46**: The title of the export. A summary of what your export does.
* **Line 51**: The list of roles requires to execute this export.
* **Line 56**: We initiate the query here...
* **Line 59**: We have to filter the query with centers the users checked in the form. We process the $acl variable to get all ``Center`` object in one array
* **Line 63**: We create the query, with a query builder.
* **Line 74**: We simply returns the result, but take care of hydrating the results as an array.
* **Line 103**: return the list of formatters types which are allowed to apply on this filter

Filters
-------

This is an example of the *filter by birthdate*. This filter ask some information in a form (`buildForm` is not empty), and this form must be validated. To performs this validations, we implement a new Interface: :class:`Chill\MainBundle\Export\ExportElementValidatedInterface`:

.. literalinclude:: /_static/code/exports/BirthdateFilter.php
   :language: php

.. todo::

   Continue to explain the export framework

.. _main bundle: https://git.framasoft.org/Chill-project/Chill-Main
